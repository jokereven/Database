## `Redis数据库下载与🐟安装✊🏿`
> 这里是根据菜鸟教程的文档✍的

菜鸟教程☘️：[菜鸟教程地址⛄](https://www.runoob.com/redis/redis-install.html)

```
这里就安装windows的其它的到菜鸟教程上面去看🏈
```
下载地址☘️：[下载地址☘️](https://github.com/tporadowski/redis/releases)

```
把压缩包.zip文件下载到你电脑的一个位置🏈解压后，将文件夹重新命名为 redis。(建议搞到C盘)

打开一个 cmd 窗口 使用 cd 命令切换目录到 C:\redis 运行:

redis-server.exe redis.windows.conf(启动服务端🍗)

没报错出现了一个节目你成功了❤️;

这时候另启一个 cmd 窗口，原来的不要关闭，不然就无法访问服务端了。

切换到 redis 目录下运行:

redis-cli.exe -h 127.0.0.1 -p 6379(启动客户端🍜)|redis-cli 直接启动

看是否连接成功输入L:✔PING
✔:出现PONG(连接成功)

远程连接
redis-cli -h host -p port -a password
h(主机) h(端口号) -a(密码)
设置键值对:
set myKey name

取出键值对:
get myKey

"name";

结束了❤️;
```
