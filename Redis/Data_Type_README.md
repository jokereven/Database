## `Redis 数据类型🥱`

```
Redis支持五种数据类型：string（字符串），hash（哈希），list（列表），set（集合）及zset(sorted set：有序集合)。
```
## `🗨这里只是学一下基础不把🙅‍所有的命令都敲一遍以后需要在啃一遍`
菜👎鸟教程Redis：[https://www.runoob.com/redis/redis-tutorial.html](https://www.runoob.com/redis/redis-tutorial.html)

### String一个Key对应一个Value☀️;
```
127.0.0.1:6379> SET NAME KIKU
OK
127.0.0.1:6379> GET NAME
"KIKU"

注意：一个键最大能存储 512MB。
```

### Hash（哈希）💘
Redis hash 是一个键值(key=>value)对集合。💘

Redis hash 是一个 string 类型的 field 和 value 的映射表，hash 特别适合用于存储对象。💘
```
127.0.0.1:6379> HMSET Kiku name "Kiku" age 32
OK
127.0.0.1:6379> HGET Kiku name
"Kiku"
127.0.0.1:6379> HGET Kiku age
"32"
127.0.0.1:6379>

实例中我们使用了 Redis HMSET, HGET 命令，HMSET 设置了两个 field=>value 对, HGET 获取对应 field 对应的 value。

每个 hash 可以存储 232 -1 键值对（40多亿）。
```


### List（列表）🍗
Redis 列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）。
```
127.0.0.1:6379> DEL Kiku
(integer) 1
127.0.0.1:6379> lpush Skill go
(integer) 1
127.0.0.1:6379> lpush Skill gin
(integer) 2
127.0.0.1:6379> lpush Skill react
(integer) 3
127.0.0.1:6379> lpush Skill vue
(integer) 4
127.0.0.1:6379> lrange Skill 0 5
1) "vue"
2) "react"
3) "gin"
4) "go"
127.0.0.1:6379>
```

### Set（集合）🍗
Redis 的 Set 是 string 类型的无序集合。

集合是通过哈希表实现的，所以添加，删除，查找的复杂度都是 O(1)。
```
127.0.0.1:6379> DEL Skill
(integer) 1
127.0.0.1:6379> sadd Skill redis
(integer) 1
127.0.0.1:6379> sadd Skill mongodb
(integer) 1
127.0.0.1:6379> sadd Skill vue
(integer) 1
127.0.0.1:6379> sadd Skill MySQL
(integer) 1
127.0.0.1:6379> sadd Skill MySQL
(integer) 0
127.0.0.1:6379> smembers Skill
1) "vue"
2) "MySQL"
3) "mongodb"
4) "redis"
127.0.0.1:6379>
```

### zset(sorted set：有序集合)🍗
Redis zset 和 set 一样也是string类型元素的集合,且不允许重复的成员。
不同的是每个元素都会关联一个double类型的分数。redis正是通过分数来为集合中的成员进行从小到大的排序。

zset的成员是唯一的,但分数(score)却可以重复。
```
127.0.0.1:6379> DEL Skill
(integer) 0
127.0.0.1:6379> zadd Skill 0 res
(integer) 1
127.0.0.1:6379> DEL Skill
(integer) 1
127.0.0.1:6379> zadd Skill 0 React
(integer) 1
127.0.0.1:6379> zadd Skill 0 Vue
(integer) 1
127.0.0.1:6379> zadd Skill 0 gin
(integer) 1
127.0.0.1:6379> zadd Skill 0 Vue
(integer) 0
127.0.0.1:6379> ZRANGEBYSCORE Skill 0 100
1) "React"
2) "Vue"
3) "gin"
127.0.0.1:6379>
```