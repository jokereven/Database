#### DDL 语句 🐟

> DDL(Data Definition Language)数据定义语言
> 用来定义数据库对象：数据库，表，列等。关键字：create, drop,alter等

## 🃏创建数据库🃏 

- ☃️首先要连接数据库
```
mysql -uroot -p
Enter passwd:******
```

☃️1. 直接创建数据库
```
CREATE DATABASE 数据库名;

//create(创造) database(数据库)
```

例子✨
```
mysql> CREATE DATABASE db1;
Query OK, 1 row affected (0.01 sec)
```

☃️2. 判断是否存在并创建数据库
```
CREATE DATABASE IF NOT EXISTS 数据库名;

//create(创造) databasse(数据库) exists(存在)
```

例子✨
```
mysql> CREATE DATABASE IF NOT EXISTS db1;
Query OK, 1 row affected, 1 warning (0.00 sec)

mysql> CREATE DATABASE IF NOT EXISTS db2;
Query OK, 1 row affected (0.01 sec)

//不管有没有创建成功都不会报错🏈;
```

☃️3. 创建数据库并指定字符集(编码表)
```
CREATE DATABASE 数据库名 CHARACTER SET 字符集;

//create(创造) database(数据库) character(性格 特色)
```

例子✨
```
mysql> CREATE DATABASE db3 CHARACTER SET gbk;
Query OK, 1 row affected (0.00 sec)
```

## 🃏查看数据库🃏 

☃️1. 查看所有的数据库
```
SHOW DATABASES;

database(数据库)
```

例子✨
```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| db1                |
| db2                |
| db3                |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
7 rows in set (0.00 sec)
```

☃️2. 查看某个数据库的定义信息
```
SHOW CREATE DATABASE 数据库名;

creata(定义) database(数据库)
```

例子✨
```
mysql> SHOW CREATE DATABASE db1;
+----------+-------------------------------------------------------------------------------------------------------------------------------+
| Database | Create Database                                                                                                               |
+----------+-------------------------------------------------------------------------------------------------------------------------------+
| db1      | CREATE DATABASE `db1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */ |
+----------+-------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.01 sec)
```

## 🃏修改数据库🃏

☃️1. 修改数据库字符集格式🐱
```
ALTER DATABASE 数据库名 DEFAULT CHARACTERR SET 字符集;

alter(改变) database(数据库) default(默认) character(性格 特色)
```   

例子✨
```
mysql> SHOW CREATE DATABASE db1;
+----------+-------------------------------------------------------------------------------------------------+
| Database | Create Database                                                                                 |
+----------+-------------------------------------------------------------------------------------------------+
| db1      | CREATE DATABASE `db1` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */ |
+----------+-------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> ALTER DATABASE db1 DEFAULT CHARACTER SET gbk;
Query OK, 1 row affected (0.01 sec)

mysql> SHOW CREATE DATABASE db1;
+----------+------------------------------------------------------------------------------------------------+
| Database | Create Database                                                                                |
+----------+------------------------------------------------------------------------------------------------+
| db1      | CREATE DATABASE `db1` /*!40100 DEFAULT CHARACTER SET gbk */ /*!80016 DEFAULT ENCRYPTION='N' */ |
+----------+------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

mysql>
```

## 🃏删除数据库🃏

```
DROP DATABASE 数据库名;
```

例子✨
```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| db1                |
| db2                |
| db3                |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
7 rows in set (0.01 sec)

mysql> DROP DATABASE db1;
Query OK, 0 rows affected (0.01 sec)

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| db2                |
| db3                |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
6 rows in set (0.00 sec)
```

## 🃏使用数据库🃏
```
1. 查看正在使用的数据库
SELECT DATABASE();

2. 使用/切换数据库
USE 数据库名;
```

例子✨
```
mysql> SELECT DATABASE();
+------------+
| DATABASE() |
+------------+
| NULL       |
+------------+
1 row in set (0.00 sec)

mysql> USE db2;
Database changed
mysql> SELECT DATABASE();
+------------+
| DATABASE() |
+------------+
| db2        |
+------------+
1 row in set (0.00 sec)
```