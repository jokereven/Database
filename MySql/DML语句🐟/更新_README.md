## 更新表记录💌

1. 不带条件修改数据🧭
```
UPDATE 表名 SET 字段名=值;
```
2. 带条件修改数据🧭
```
UPDATE 表名 SET 字段名=值 WHERE 字段名=值;
```

`关键字说明`⌛

```
UPDATE: 修改数据
SET: 修改哪些字段
WHERE: 指定条件
具体操作：
```

不带条件修改数据，将所有的性别改成女📌

```
UPDATE student SET sex='女';
```

带条件修改数据，将 id 号为 2 的学生性别改成男📌

```
UPDATE student SET sex='男' WHERE id=2;
```

一次修改多个列，把 id 为 3 的学生，年龄改成 26 岁，address 改成北京📌

```
UPDATE student SET age=26, address='北京' WHERE id=3;
```
