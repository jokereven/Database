## 删除表记录📌

1. 不带条件删除数据📌

```
DELETE FROM 表名;
```

2. 带条件删除数据📌

```
DELETE FROM 表名 WHERE 字段名=值;
```

具体操作：

`带条件删除数据，删除 id 为 3 的记录📌`
```
DELETE FROM student WHERE id=3;
```
`不带条件删除数据,删除表中的所有数据📌`
```
DELETE FROM student;
```
truncate 删除表记录📌

`TRUNCATE TABLE 表名;`

`truncate 和 delete 的区别：`
```
delete 是将表中的数据一条一条删除📌

truncate 是将整个表摧毁，重新创建一个新的表,新的表结构和原来表结构一模一样📌
```