## 蠕虫复制
```
什么是蠕虫复制：

在已有的数据基础之上，将原来的数据进行复制，插入到对应的表中

语法格式：INSERT INTO 表名 1 SELECT * FROM 表名 2;

作用:将表名 2 中的数据复制到表名 1 中
```

`具体操作:`

`创建 student2 表，student2 结构和 student 表结构一样`
```
CREATE TABLE student LIKE student2;
```
`将 student 表中的数据添加到 student2 表中`
```
INSERT INTO student SELECT * FROM student2;
```

`注意：如果只想复制 student 表中 name,age 字段数据到 student2 表中使用如下格式`
```
INSERT INTO student2(name,age) SELECT name,age FROM student;
```