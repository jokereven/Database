## DML 语句 🥮
```
DML(Data Manipulation Language)数据操作语言

用来对数据库中表的数据进行增删改。关键字：insert, delete, update 等
```
```
INSERT INTO 表名 – 表示往哪张表中添加数据
```
```
(字段名 1, 字段名 2, …) -- 要给哪些字段设置值

VALUES (值 1, 值 2, …); -- 设置具体的值
```
`注意`

```
值与字段必须对应，个数相同，类型相同
值的数据大小必须在字段的长度范围内
除了数值类型外，其它的字段类型的值必须使用引号引起。（建议单引号）
如果要插入空值，可以不写字段，或者插入null
```

1. `插入全部字段 🥟`

- `所有的字段名都写出来`
```
INSERT INTO 表名 (字段名 1, 字段名 2, 字段名 3…) VALUES (值 1, 值 2, 值 3);
```
- `不写字段名`
```
  INSERT INTO 表名 VALUES (值 1, 值 2, 值 3…);
 ```
2. `插入部分数据🥟`
```
  INSERT INTO 表名 (字段名 1, 字段名 2, ...) VALUES (值 1, 值 2, ...);
```
  `没有添加数据的字段会使用 NULL`

`具体操作:`
```
创建 db2 数据库，并使用。

CREATE DATABASE db2;

USE db2;
```
```
创建完整学生信息表，包括学员的 id，姓名，年龄，性别，家庭地址，电话号码，生日，数学成绩，英语成绩
```
```
CREATE TABLE student(

id int, name varchar(20), age int, sex char(1), address varchar(200),
phone varchar(20), birthday date, math double, english double
);
```

`插入部分数据，往学生表中添加 id, name, age, sex,address 数据`
```
INSERT INTO student (id,name,age,sex,address) values(1,'张三',19,'男','北京市');
```
`提示：使用 SELECT \* FROM 表名；测试是否插入成功`

`向表中插入所有字段`

`所有的字段名都写出来`
```
INSERT INTO student(id, name, age, sex, address, phone, birthday, math, english) values(2,'小美',18,'女','上海市','18888888888','2011-12-12',65.5,99.2);
```
`不写字段名`
```
INSERT INTO student values(3,'小明',27,'男','深圳市','13333333333','2000-11-06',95.5,92);
```