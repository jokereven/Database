#### SQL 的概念

1. 什么是 SQL

```
	结构化查询语言(Structured Query Language)简称SQL,SQL语句就是对数据库进行操作的一种语言。
```

2. SQL 作用

```
通过SQL语句我们可以方便的操作数据库中的数据库、表、数据。
​ SQL是数据库管理系统都需要遵循的规范。不同的数据库生产厂商都支持SQL语句，但都有特有内容。
```

![](https://gitee.com/jokereven/images/raw/master/PicGo/20210508134258.jpg)

3. SQL 语句分类

```
DDL(Data Definition Language)数据定义语言
用来定义数据库对象：数据库，表，列等。关键字：create, drop,alter等

DML(Data Manipulation Language)数据操作语言
用来对数据库中表的数据进行增删改。关键字：insert, delete, update等

DQL(Data Query Language)数据查询语言
用来查询数据库中表的记录(数据)。关键字：select, where等

DCL(Data Control Language)数据控制语言(了解)
用来定义数据库的访问权限和安全级别，及创建用户。关键字：GRANT， REVOKE等
```
