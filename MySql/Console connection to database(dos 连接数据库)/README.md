#### 控制台连接数据库

1. `登录格式1：mysql -u用户名 -p密码`

```
  例如：mysql –uroot -proot(这里输入密码明文)
```

`后输入密码方式：`

```
mysql -u用户名 -p回车

密码(密文)
```

2. `登录格式2：mysql -hip地址 -u用户名 -p密码`

```
  例如：mysql –h127.0.0.1(localhost) –uroot -proot

  or mysql -hlocalhost -uroot -p
  密码:
```

3. `登录格式3：mysql --host=ip地址 --user=用户名 --password=密码`

```
例如：mysql --host=localhost --user=root --password=root(与上面一样)
```

4. `退出MySQL：exit`
