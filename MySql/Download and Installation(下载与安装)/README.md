#### `MySQL Download and Installation`

[下载与安装教程](https://www.runoob.com/w3cnote/windows10-mysql-installer.html)

[直接下载地址](https://dev.mysql.com/downloads/windows/installer/)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/140526_1480953e_8837544.png "download to check.png")
```
这里选择的是安装版（mysql -install-community）;

选择不登陆下载;

下载之后就是安装了废话不多说;

注意一点;
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/140557_af27307c_8837544.png "Server only.png")

```
选择这个安装其他的都是下一步

安装完成到了环境配置

在系统环境的path下加一个(C:\Program Files\MySQL\MySQL Server 8.0\bin)

安装完成之后测试一下
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/140613_58edd4d2_8837544.png "mysql -u root -q.png")
