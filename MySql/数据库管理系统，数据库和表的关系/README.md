#### 数据库管理系统、数据库和表的关系

```
数据库管理系统（DataBase Management System，DBMS）：指一种操作和管理数据库的大型软件，用于建立、使用和维护数据库，对数据库进行统一管理和控制，以保证数据库的安全性和完整性。用户通过数据库管理系统访问数据库中表内的数据
```

```
数据库管理程序(DBMS)可以管理多个数据库，一般开发人员会针对每一个应用创建一个数据库。为保存应用中实体的数据，一般会在数据库创建多个表，以保存程序中实体的数据。数据库管理系统、数据库和表的关系如图所示：
```

<img src="https://gitee.com/jokereven/images/raw/master/PicGo/20210508133516.png" style="zoom: 80%;" />

```
先有数据库 → 再有表 → 再有数据

一个库包含多个表
```

