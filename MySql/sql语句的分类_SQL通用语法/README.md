#### sql 语句分类\_SQL 通用语法

## sql 语句分类

1. DDL(Data Definition Language)数据定义语言
   用来定义数据库对象：数据库，表，列等。关键字：create, drop,alter 等

2. DML(Data Manipulation Language)数据操作语言
   用来对数据库中表的数据进行增删改。关键字：insert, delete, update 等

3. DQL(Data Query Language)数据查询语言
   用来查询数据库中表的记录(数据)。关键字：select, where 等

4. DCL(Data Control Language)数据控制语言(了解)
   用来定义数据库的访问权限和安全级别，及创建用户。关键字：GRANT， REVOKE 等

## SQL 通用语法

SQL 语句可以单行或多行书写，以分号结尾。

可使用空格和缩进来增强语句的可读性。

MySQL 数据库的 SQL 语句不区分大小写，关键字建议使用大写。

```
SELECT * FROM student;
select * from student;
SELECT * FROM student;
```
