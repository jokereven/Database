#### 数据库的启动

`MySQL启动方式和普通的windows程序双击启动方式不同，分为以下3种：`

1. `Windows服务方式启动`

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/152750_fdb08373_8837544.png "1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/152803_c48d2579_8837544.png "2.png")

2. `命令方式启动`

```
windows+r键调出运行窗口，输入services.msc命令。

随后在服务中找到MySQL80服务启动即可
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/152827_f2d9cc31_8837544.png "3.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/152838_f7cbbffb_8837544.png "4.png")


3. `以管理员身份运行cmd打开dos窗口`

```
输出net start mysql80

停止服务 net stop mysql80
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/152850_8f5a4f26_8837544.png "5.png")

