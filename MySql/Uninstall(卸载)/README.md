#### 数据库的卸载

1. `打开电脑的服务窗口，关闭MySQL服务并且卸载一切与mysql相关的模块`

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/144254_9d462506_8837544.png '1.png')

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/144312_ff37e0a5_8837544.png '2.png')

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/144323_ce715835_8837544.png '3.png')

2. `找到MySQL的安装目录，查找是否还残留相关文件夹，如果有删除即可`

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/144515_e4d7bab8_8837544.png '4.png')

3. `勾选显示文件夹选项`

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/144624_d697eeb9_8837544.png '5.png')

4. `找到ProgramDate文件夹，删除里面mysql文件夹即可`

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/144904_3ed996b3_8837544.png '6.png')
