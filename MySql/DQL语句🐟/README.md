## DQL🚰
```
DQL(Data Query Language)数据查询语言

用来查询数据库中表的记录(数据)。

关键字：select, where等
```
>`注意：查询不会对数据库中的数据进行修改.只是一种显示数据的方式`

### 简单查询📌

`为student准备数据`
```
INSERT INTO student values(1,'闫妮',43,'女','北京市','12222222222','2019-02-12',92.5,88);
INSERT INTO student values(2,'郭富城',21,'男','上海市','1666666666','2018-06-06',97.5,65.5);
INSERT INTO student values(3,'赵丽颖',44,'女','深圳市','13333333333','2012-07-15',42.5,74.5);
INSERT INTO student values(4,'张学友',34,'男','杭州市','17777777777','2013-11-17',69,65);
INSERT INTO student values(5,'成龙',51,'男','哈尔滨市','15555555555','2005-10-12',88,97);
INSERT INTO student values(6,'刘德华',57,'男','盘锦市','19999999999','2015-11-11',74.5,92.5);
INSERT INTO student values(7,'马伊琍',42,'女','长沙市','18888888888','2008-03-26',86.5,71.5);
INSERT INTO student values(8,'黎明',49,'男','昆明市','11111111111','2000-09-14',77.5,60);
```
1. `查询表所有数据📌`

`使用*表示所有列`

```
SELECT * FROM 表名;
```

`具体操作：`

```
SELECT * FROM student;
```

`写出查询每列的名称📌`

```
SELECT 字段名1, 字段名2, 字段名3, ... FROM 表名;
```

具体操作：

```
SELECT id,name,age,sex,address,phone,birthday,math,english FROM student;
```

2. `查询指定列📌`

`查询指定列的数据,多个列之间以逗号分隔`

```
SELECT 字段名1, 字段名2... FROM 表名;
```

`具体操作：`

`查询student表中的name 和 age 列📌`
```
SELECT NAME, age FROM student;
```
3. `别名查询📌`
```
查询时给列、表指定别名需要使用AS关键字📌

使用别名的好处是方便观看和处理查询到的数据📌
```
```
SELECT 字段名1 AS 别名, 字段名2 AS 别名... FROM 表名;
```

`注意：AS可以省略不写📌`

`具体操作：`

`查询sudent表中name 和 age 列，name列的别名为”姓名”，age列的别名为”年龄”`
```
SELECT NAME AS 姓名, age AS 年龄 FROM student;
```
4. `清除重复值📌`
`查询指定列并且结果不出现重复数据📌`

```
SELECT DISTINCT 字段名 FROM 表名;
```
`补充数据`
```
INSERT INTO student values(9,'黎明',49,'女','锦州市','14444444444','2000-06-18',73.5,69);
INSERT INTO student values(10,'黎明',31,'女','重庆市','10000000000','2010-05-23',63.5,88);
```
`具体操作：`

`查询name，age列并且结果不出现重复name📌`
```
SELECT DISTINCT NAME, age FROM student;
```
5. `查询结果参与运算📌`

`某列数据和固定值运算`
```
SELECT 列名1 + 固定值 FROM 表名;
```
`某列数据和其他列数据参与运算📌`
```
SELECT 列名1 + 列名2 FROM 表名;
```
`注意: 参与运算的必须是数值类型📌`

`需求：`

`查询的时候将数学和英语的成绩相加`

`让学员的年龄增加10岁`
`实现：`

`查询math + english的和`
```
SELECT math + english FROM student;
```
>`结果确实将每条记录的math和english相加，但是效果不好看📌`

`查询math + english的和使用别名”总成绩”`
```
SELECT math + english 总成绩 FROM student;
```
`查询所有列与math + english的和并使用别名”总成绩”`
```
SELECT *, math + english 总成绩 FROM student;
```
`查询姓名、年龄，将每个人的年龄增加10岁📌`
```
SELECT name, age + 10 FROM student;
```