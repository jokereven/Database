关注行业前沿，分享所见所学，持续输出优质文章 🚀;

未来可期 🏂;

您好！我是周靖（🏂JokerEven🏂），欢迎来到我的博客！

我会定期发布一些大前端相关的前沿技术文章和日常开发过程中的实战总结。主要还是想和大家一起进步、一起成长。😜

🏂MySQL🏂;

下载与安装🏂：[MySQL Download and Installation](https://gitee.com/jokereven/Database/tree/master/MySql/Download%20and%20Installation(%E4%B8%8B%E8%BD%BD%E4%B8%8E%E5%AE%89%E8%A3%85))

卸载🏂：[Uninstall](https://gitee.com/jokereven/Database/tree/master/MySql/Uninstall(%E5%8D%B8%E8%BD%BD))

数据库的启动🏂：[Start MySQL](https://gitee.com/jokereven/Database/tree/master/MySql/Start%20MySQL(%E5%90%AF%E5%8A%A8MySQL))

Dos面板 控制台连接数据库🏂：[Console connection to database](https://gitee.com/jokereven/Database/tree/master/MySql/Console%20connection%20to%20database(dos%20%E8%BF%9E%E6%8E%A5%E6%95%B0%E6%8D%AE%E5%BA%93))

SQL的概述以及分类🏂：[Structured Query Language](https://gitee.com/jokereven/Database/tree/master/MySql/SQL%E7%9A%84%E6%A6%82%E8%BF%B0%E4%BB%A5%E5%8F%8A%E5%88%86%E7%B1%BB)

数据库管理系统、数据库和表的关系🏂：[DataBase Management System，DBMS](https://gitee.com/jokereven/Database/tree/master/MySql/%E6%95%B0%E6%8D%AE%E5%BA%93%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F%EF%BC%8C%E6%95%B0%E6%8D%AE%E5%BA%93%E5%92%8C%E8%A1%A8%E7%9A%84%E5%85%B3%E7%B3%BB)

🐟DDL🏂语句：🏂：[DDL(操作数据库)](https://gitee.com/jokereven/Database/tree/master/MySql/DDL%E8%AF%AD%E5%8F%A5%F0%9F%90%9F(%E6%93%8D%E4%BD%9C%E6%95%B0%E6%8D%AE%E5%BA%93%F0%9F%93%95))
🏂：[DDL(操作表&列)](https://gitee.com/jokereven/Database/tree/master/MySql/DDL%E8%AF%AD%E5%8F%A5%F0%9F%90%9F(%E6%93%8D%E4%BD%9C%E8%A1%A8%E5%92%8C%E5%88%97%F0%9F%93%95))

🐟DML语句🏂：[DML📌增删改📌](https://gitee.com/jokereven/Database/tree/master/MySql/DML%E8%AF%AD%E5%8F%A5%F0%9F%90%9F)

🐟DQL🚰语句🏂：[查📌](https://gitee.com/jokereven/Database/tree/master/MySql/DQL%E8%AF%AD%E5%8F%A5%F0%9F%90%9F)

以上就是MySQL的基本使用了📌

修改密码📌
```
mysqladmin -u用户名 -p旧密码 password 新密码
```

加油💪

[MySQL及MySQLworkbench安装教程💻](https://blog.csdn.net/liuzuoping/article/details/101931559)

[MySQLworkbench汉化📌](https://github.com/pifeifei/mysql-workbench-zh-cn)