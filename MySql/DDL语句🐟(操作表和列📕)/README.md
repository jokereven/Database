#### 操作表和列🏈

## 创建表🎅
```
表的结构与excel相同✊🏿

语法🐟:

CREATE TABLE 表名 (字段名1 字段类型1, 字段名2 字段类型2…);
```
`MySQL数据类型🎅`
| int     | 整数       |
|---------|----------|
| double  | 浮点数      |
| varchar | 字符串      |
| data    | 日期 年-月-日 |

`🎅还有详细数据类型这里就不写了需要的时候去百度🎅;`

### 🎅创建stuednt表单
```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| db2                |
| db3                |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
6 rows in set (0.00 sec)

mysql> SELECT DATABASE();
+------------+
| DATABASE() |
+------------+
| NULL       |
+------------+
1 row in set (0.00 sec)

mysql> USE db3;
Database changed
mysql> SELECT DATABASE();
+------------+
| DATABASE() |
+------------+
| db3        |
+------------+
1 row in set (0.00 sec)

mysql> CREATE TABLE student(id int,name varchar(10),birthday date);
Query OK, 0 rows affected (0.04 sec)
```

### 🎅查看表

1. 查看一个数据库中所有的表🎿;
```
SHOW TABLES;
```

测试🎿
```
mysql> SHOW TABLES;
+---------------+
| Tables_in_db3 |
+---------------+
| student       |
+---------------+
1 row in set (0.02 sec)
```

2. 查看表结构🎿;
```
DESC 表名;
```

测试🎿
```
mysql> DESC student;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| id       | int         | YES  |     | NULL    |       |
| name     | varchar(10) | YES  |     | NULL    |       |
| birthday | date        | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)
```

3. 查看创建表的SQL语句🎿;
```
SHOW CREATE TABLE 表名;
```

测试🎿
```
mysql> SHOW CREATE TABLE student;
+---------+---------------------------------------------------------------------------------------------------------------------------------------------------------+
| Table   | Create Table                                                                                                                                            |
+---------+---------------------------------------------------------------------------------------------------------------------------------------------------------+
| student | CREATE TABLE `student` (
  `id` int DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  `birthday` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk |
+---------+---------------------------------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)
```

4. 快速创建一个表结构相同的表🎿;
```
CREATE TABLE 新表名 LIKE 旧表名;
```

测试🎿
```
mysql> CREATE TABLE student1 LIKE student;
Query OK, 0 rows affected (0.04 sec)

mysql> SHOW TABLES;
+---------------+
| Tables_in_db3 |
+---------------+
| student       |
| student1      |
+---------------+
2 rows in set (0.00 sec)

mysql> DESC student1;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| id       | int         | YES  |     | NULL    |       |
| name     | varchar(10) | YES  |     | NULL    |       |
| birthday | date        | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> DESC student;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| id       | int         | YES  |     | NULL    |       |
| name     | varchar(10) | YES  |     | NULL    |       |
| birthday | date        | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)
```

5. 删除表🎿;
    1. 直接删除表🎿
        ```
        DROP TABLE 表名;
        ```
    2. 判断是否存在并删除表🎿
        ```
        DROP TABLE IF EXISTS 表名;
        ```

6. 修改表结构🎿;
> 修改表结构使用不是很频繁，只需要了解，等需要使用的时候再回来查即可

    1. 添加列表🍺
    ```
    ALTER TABLE 表名 ADD 列名 类型;
    ```
    
    2. 修改列类型🍺
    ```
    ALTER TABLE 表名 MODIFY列名 新的类型;
    ```

    3. 修改列名🍺
    ```
    ALTER TABLE 表名 CHANGE 旧列名 新列名 类型;
    ```

    4. 删除列🍺
    ```
    ALTER TABLE 表名 DROP 列名;
    ```

    5. 修改表名🍺
    ```
    RENAME TABLE 表名 TO 新表名;
    ```
    
    6. 修改字符集🍺
    ```
    ALTER TABLE 表名 character set 字符集;
    ```