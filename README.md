关注行业前沿，分享所见所学，持续输出优质文章 🚀;

未来可期 🏂;

您好！我是周靖（🏂JokerEven🏂），欢迎来到我的博客！

我会定期发布一些大前端相关的前沿技术文章和日常开发过程中的实战总结。主要还是想和大家一起进步、一起成长。😜

数据库📕学习🚀;

SQL🏂：[MySQL](https://gitee.com/jokereven/Database/tree/master/MySql)

NO🏂SQL🚀：[Redis](https://gitee.com/jokereven/Database/tree/master/Redis)